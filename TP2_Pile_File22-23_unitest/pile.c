/**
 * @file pile.c
 * @brief fichier d'implementation pour la gestion de pile
 */
 
#include <stdlib.h>
#include <stdio.h>
#include "pile.h"

/** 
 * @brief Initialiser une pile du type pile_t
 *   - allocation de memoire pour une structure pile_t et un tableau de taille elements
 * @param [in] taille taille de la pile
 * @return l'adresse de la structure
 */
pile_t * initPile(int taille)
{
    pile_t * pile = NULL;

    if( taille > 0 )
    {
        pile            = malloc(sizeof(pile_t));

        pile->taille    = taille;
        pile->sommet    = -1;
        pile->base      = malloc(taille * sizeof(eltType));
    }

    return pile;
}


/** 
 * @brief Liberer les memoires occupees par la pile
 * @param [in, out] adrPtPile l'adresse du pointeur de la structure pile_t
 */
void libererPile(pile_t ** adrPtPile)
{
    *adrPtPile = NULL;
}


/** 
 * @brief Verifier si la pile est vide (aucun element dans la "base")
 * @param [in] ptPile l'adresse de la structure pile_t
 * @return 1 - vide, ou 0 - non vide
 */
int estVidePile(pile_t * ptPile)
{
    int retour = 0;

    if ( ptPile->sommet == -1 )
    {
        retour = 1;
    }

    return retour;
}


/** TO DO
 * @brief Entrer un element dans la pile
 * @param [in, out] ptPile l'adresse de la structure pile_t
 * @param [in] ptVal l'adresse de la valeur a empiler
 * @param [in, out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void empiler(pile_t * ptPile, eltType * ptVal, int * code)
{
    *code = 1;

    if( ptPile->sommet < (ptPile->taille) - 1 )
    {
        ptPile->sommet ++;
        copyElt(ptVal, &ptPile->base[ptPile->sommet] );
        *code = 0;        
    }

}


/** 
 * @brief Sortir un element de la pile
 * @param [in, out] ptPile l'adresse de la structure pile_t
 * @param [out] ptRes l'adresse de l'element sorti
 * @param [in, out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void depiler(pile_t * ptPile, eltType * ptRes, int * code)
{
    *code = 1;

    if( estVidePile(ptPile) != 1 )
    {
        copyElt(&ptPile->base[ptPile->sommet], ptRes);
        ptPile->sommet --;
        *code = 0;        
    }
}


/** 
 * @brief Affiche tout le tableau avec une indication au sommet de la pile
 * @param [in] ptPile l'adresse de la structure pile_t
 */
void affichePile(pile_t * ptPile)
{
    int         i;
    eltType *   cour;
    
    i = 0;
    cour = ptPile->base + i;

    printf("\n----------------PILE----------------\n");
    while (i != ptPile->taille)
    {
        // printf("element %d : %c et %d\n", i, cour->lettre, cour->num);
        printf("element %d : %d\n", i, *cour);
        if( i == ptPile->sommet )
        {
            printf("sommet\n");
            ptPile->taille --;
        }
        i ++;
        cour = ptPile->base + i;
    }
    printf("------------------------------------\n\n");
}


