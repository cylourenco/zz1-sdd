/**
 * @file file.c
 * @brief fichier d'implementation pour la gestion de file
 */
#include <stdlib.h>
#include <stdio.h>
#include "file.h"

/**  
 * @brief Initialiser une file du type eltType
 *   - allocation de memoire pour une structure file_t et un tableau de taille elements
 * @param [in] taille taille de la file
 * @return l'adresse de la structure file_t
 */
file_t *  initFile(int taille)
{
    file_t * file = NULL;

    if( taille > 0 )
    {
        file            = malloc(sizeof(file_t));

        file->taille    = taille;
        file->nbElts    = 0;
        file->base      = malloc(taille * sizeof(eltType));
        file->deb       = file->base;
        file->fin       = file->base + file->taille - 1;
    }

    return file;
}


/**  
 * @brief Verifier si la file est vide (aucun element dans la file)
 * @param [in] ptFile l'adresse de la structure file_t
 * @return 1 - vide, ou 0 - non vide
 */
int  estVideFile(file_t * ptFile)
{
    int retour = 0;

    if( ptFile->nbElts == 0 )
    {
        retour = 1;
    }

    return retour;
}


/** 
 * @brief Verifier si la file est pleine
 * @param [in] ptFile l'adresse de la structure file_t
 * @return 1 - pleine, ou 0 - pas pleine
 */
int estPleineFile(file_t * ptFile)
{
    int retour = 0;

    if( ptFile->nbElts > (ptFile->taille) - 1 )
    {
        retour = 1;
    }

    return retour;
}


/** 
 * @brief Liberer les memoires occupees par la file
 * @param [in, out] adrPtFile l'adresse du pointeur de la structure file_t
 */
void libererFile(file_t ** adrPtFile)
{
    *adrPtFile = NULL;
}


/** 
 * @brief Entrer un element dans la file
 * @param [in] ptFile le pointeur de tete de la file
 * @param [in] ptVal l'adresse de la valeur a empiler
 * @param [out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void entrerFile(file_t * ptFile, eltType * ptVal, int * code)
{
    *code = 1;

    if( estPleineFile(ptFile) != 1 )
    {
        if( ptFile->fin == ptFile->base + ptFile->taille - 1 )
        {
            ptFile->fin = ptFile->base;
        }

        else
        {
            ptFile->fin = ptFile->fin + 1;
        }

        copyElt(ptVal, &ptFile->base[ptFile->nbElts]);
        copyElt(&ptFile->base[ptFile->nbElts], ptFile->fin );
        ptFile->nbElts ++;

        *code = 0;
    }
}


/** 
 * @brief Sortir un element de la file
 * @param [in] ptFile le pointeur de tete d'une file
 * @param [out] ptRes l'adresse de l'element sorti
 * @param [out] code : l'adresse du code de sortie
 *                     *code = 0 si reussi
 *                           = 1 si echec
 */
void sortirFile(file_t * ptFile, eltType * ptRes, int * code)
{
    *code = 1;

    if( estVideFile(ptFile) == 0 )
    {
        copyElt(ptFile->deb, ptRes);

        if ( ptFile->deb == ptFile->base + ptFile->taille - 1 )
        {
            ptFile->deb = ptFile->base;
        }

        else
        {
            ptFile->deb = ptFile->deb + 1;
        }

        ptFile->nbElts --;

        *code = 0;
    }
}


/** 
 * @brief Affiche la file
 * @param [in] ptPile l'adresse de la structure pile_t
 */
// affiche la file
void afficheFile(file_t * ptFile)
{
    int         i;
    eltType *   cour;
    
    i = 0;
    cour = ptFile->base + i;

    printf("\n----------------FILE----------------\n");
    while (i != ptFile->taille)
    {
        if (cour == ptFile->deb)
        {
            printf("debut\n");
        }
        printf("element %d : %c et %d\n", i, cour->lettre, cour->num);
        if (cour == ptFile->fin)
        {
            printf("fin\n");
        }
        i ++;
        cour = ptFile->base + i;
    }
    printf("------------------------------------\n\n");
}

// affiche tout le tableau avec un indicateur au début et à la fin de la file
// void afficheFile(file_t * ptFile)
// {
//     int         i;
//     eltType *   cour;
    
//     i = 0;
//     cour = ptFile->deb + i;

//     printf("\n----------------FILE----------------\n");
//     while (ptFile->deb + i != ptFile->fin + 1)
//     {
//         printf("element %d : %c et %d\n", i, cour->lettre, cour->num);
//         i ++;
//         cour = ptFile->deb + i;
//     }
//     printf("------------------------------------\n\n");
// }

