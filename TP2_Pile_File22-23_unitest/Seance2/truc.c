/**
 * @file truc.c
 * @brief fichier d'implementation du module de derecursification de la fonction truc (donne la liste de combinaisons de n elements)
 */
#include <stdio.h>
#include <stdlib.h> 

#include "../pile.h"

#define TAILLE 100

/** 
 * @brief initialiser n caracteres dans un tableau de l'indice deb a l'indice fin
 * @param [in] deb l'indice du 1er caractere
 * @param [in] fin l'indice du dernier caractere
 * @param [in, out] T : un tableau de caracteres
 */
void initTab(int deb, int fin, char T[])
{
    int i,
        ascii = 65;

    for(i = deb; i <= fin; i ++)
    {
        T[i] = ascii;
        ascii ++;
    }
}


/** 
 * @brief afficher les caracteres d'un tableau de l'indice deb a l'indice fin a l'ecran
 * @param [in] deb l'indice du 1er caractere
 * @param [in] fin l'indice du dernier caractere
 * @param [in] T : un tableau de caracteres
*/
void printTab(int deb, int fin, char T[])
{
	int i;

    for(i = deb; i <= fin; i ++)
    {
        printf("%c ", T[i]);
    }

    printf("\n");
}

/** 
 * @brief echanger les valeurs de 2 variables de type caractere
 * @param [in, out] a l'adresse de la 1ere variable 
 * @param [in, out] b l'adresse de la 2eme variable 
 */
void echangerChar(char * a, char * b)
{
	char tmp;
    
    tmp  = *a;
    *a   = *b;
    *b   = tmp;
}

/** 
 * @brief l'algorithme recursive
 * @param [in] i l'indice du 1er element (i=1 avec ce TP)
 * @param [in] n l'indice du dernier element 
 * @param [in] T tableau de n caractere de l'indice 1 a l'indice n 
 */
void truc_rec(int i, int n, char T[])
{
    int j;

	if( i == n )
    {
        printTab(1, n, T);
    }
    else
    {
        for(j = i; j <= n; j ++)
        {
            echangerChar(&T[i], &T[j]);
            truc_rec(i + 1, n, T);
            echangerChar(&T[i], &T[j]);
        }
    }
}

/** TO DO
 * @brief l'algorithme iterative
 * @param [in] i l'indice du 1er element (i=1 avec ce TP)
 * @param [in] n l'indice du dernier element 
 * @param [in] T tableau de n+1 caracteres de l'indice 0 a l'indice n 
 */
void truc_iter(int i, int n, char * T)
{
    //initialisation
    eltType vi = i;
    eltType vj = vi;


    int fin = 0;
    int code = 0;

    pile_t * ptPile = initPile(TAILLE);


    while (!fin)
    {
        if (vj <= n)
        {
            while(vi != n)
            {
                empiler(ptPile, &vi, &code);
                empiler(ptPile, &vj, &code);

                echangerChar(&T[vi], &T[vj]);

                vi += 1;
                copyElt(&vi, &vj);
            }

            printTab(1, n, T);
        }

        if (!estVidePile(ptPile))
        {
            depiler(ptPile, &vj, &code);
            depiler(ptPile, &vi, &code);

            echangerChar(&T[vi], &T[vj]);

            vj += 1;
        }
        else
        {
            libererPile(&ptPile);
            fin = 1;
        }
    }
}