/**
 * @file eltType.c
 * @brief fichier d'implementation pour la gestion du type des elements
 *        Ce module contient le type de donnees et les fonctions de gestion de piles/file
 *        L'utilisant du type 'eltType' permet aux piles/files de stocker des elements de different type
 */
#include <stdio.h>
#include "eltType.h"

/* ------------------------- Gestion des elements de type 'int' ------------------------- */
/** 
 * @brief Ecrire une fonction de comparaison de 2 elements 'int'
 * @param [in] adrElt1 l'adresse du 1er element du type 'eltType'
 * @param [in] adrElt2 l'adresse du 2eme element du type 'eltType'
 * @return 1 si les 2 elements sont egaux; 0 s'ils sont differents
 */
int compareElt(eltType * adrElt1, eltType * adrElt2)
{
	int retour = 0;

    if (*adrElt1 == *adrElt2)
    {
        retour = 1;
    }

    return retour;
}

/** 
 * @brief Copier la valeur d'un element a un autre emplacement
 * @param [in] adrEltCopier l'adresse de l'element a copier
 * @param [in] adrEltDest l'adresse de la destination
 */
void copyElt(eltType * adrEltCopier, eltType * adrEltDest)
{
	*adrEltDest = *adrEltCopier;    
}



/* ---------- Gestion des elements de type struct contenant un 'char' et un 'int' ---------- */
/** 
 * @brief Ecrire une fonction de comparaison de 2 elements
 * @param [in] xxx l'adresse du 1er element
 * @param [in] xxx l'adresse du 2eme element
 * @return 1 si les 2 elements sont egaux; 0 s'ils sont differents
 */
// int compareElt(eltType * adrElt1, eltType * adrElt2)
// {
// 	int retour = 0;

//     if ((*adrElt1).lettre == (*adrElt2).lettre && (*adrElt1).num == (*adrElt2).num)
//     {
//         retour = 1;
//     }
    
//     return retour;
// }

/** 
 * @brief Copier la valeur d'un element dans un autre emplacement
 * @param [in] xxx l'adresse de l'element a copier
 * @param [in] xxx l'adresse de la destination
 */
// void copyElt(eltType * adrEltCopier, eltType * adrEltDest)
// {
// 	(*adrEltDest).lettre    = (*adrEltCopier).lettre;
//     (*adrEltDest).num       = (*adrEltCopier).num;    
// }
