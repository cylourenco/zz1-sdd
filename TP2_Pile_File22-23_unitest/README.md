# TP2 - Pile et File 

## Pour commencer

Ce TP traite des piles et files ainsi de la dérursification d'une fonction à appel récursif grâce aux piles.

## Démarrage

Dans ce TP, il y a 2 parties :
* une partie dans [Seance1] avec Pile et File, il se compile :  
$ make  
On choisit soit pile ou file pour compiler : 
$ ./pile_main  
ou  
$ ./file_main  

* puis dans la partie [Seance2], on traite de la dérursification d'une fonction, il se compile :   
$ make     
$ ./truc_main  

## Auteurs

* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/users/cylourenco)
* **Louane MECHOUD** _alias_ [@lomechoud](https://gitlab.isima.fr/users/lomechoud)

