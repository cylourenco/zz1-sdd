/**
 * @file polynomial_main.c
 * @brief Programme pour les tests des operations sur les polynomes
 * ATTENTION : Il faut creer autant de tests que les cas d'execution pour chaque fonction a tester
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "polynomial.h"
#include "teZZt.h"

BEGIN_TEST_GROUP(polynomial)

TEST(LL_init_list) {
	cell_t *list;

	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);

	REQUIRE ( list == NULL );
}

TEST(Poly_derive) { // test sur la derivation d'un polynome

	printf("\n\nDIFFERENTIATE\n");
/* 
liste de cas:
- polynôme null
- polynôme sortie null
- polynôme quelconque
*/

	cell_t 	*poly, 		// polynôme
			*list; 		// liste qui permet d'insérer dans le polynôme

	char 	buffer[1024];
	FILE   *file = NULL;

// Dérivée un polynôme null

	printf("\nDifferenciate of a linked list null : \n");

	poly = NULL;               			// polynôme null

	poly_derive(&poly);

	CHECK( poly == NULL );

// Dériver un polynôme qui devient null

	printf("\nDifferenciate of a linked list which become null : \n");

	monom_t monom = {1.5, 0}; 						
	LL_init_list(&poly);				// ajout d'un monom de degré 0 dans le polynôme null 
	list = LL_create_cell(&monom);
	LL_add_cell(&poly, list);

	poly_derive(&poly);

	CHECK( poly == NULL );

// Dériver un polynôme quelconque 

	printf("\nDifferenciate of a linked list : \n");

	LL_create_list_fromFileName(&poly, "folder/poly3.txt");
 
 	poly_derive(&poly);

	//	vérification du polynôme avec un buffer 

	file = fmemopen(buffer, 1024, "w");

	LL_save_list_toFile(file, poly, *(monom_save2file));

	CHECK( 0 == strcmp(buffer, "1.000 1\n8.500 4\n6.000 5\n43.265 16\n"));

	// libération polynomes
	LL_free_list(&poly);
	LL_free_list(&list);

}

// Addition classique de deux polynomes
TEST(Poly_addition1)
{
	printf("\nAddition classique de deux polynomes\n");
	cell_t *poly1;
	cell_t *poly3;

	LL_init_list(&poly1);
	LL_init_list(&poly3);

	char buffer[1024];

	// Ouverture du buffer
	FILE *file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);

	// Récupération des polynôme via. fichiers
	LL_create_list_fromFileName(&poly1, "./folder/poly1.txt");
	LL_create_list_fromFileName(&poly3, "./folder/poly3.txt");

	// Addition : P1 + P2
	poly_add(&poly1, &poly3);
	LL_save_list_toFile(file, poly1, monom_save2file);

	CHECK(0 == strcmp(buffer, "1.000 0\n2.000 1\n-1.500 2\n1.000 3\n2.700 5\n1.000 6\n2.545 17\n"));
	CHECK(poly3 == NULL);

	// Liberation
	LL_free_list(&poly1);
	LL_free_list(&poly3);
}


// Addition d'un polynome non nul avec le polynome nul
TEST(Poly_addition2)
{
	printf("\nAddition d'un polynome non nul avec un polynome nul ...\n");
	cell_t *poly1;
	cell_t *poly2;

	LL_init_list(&poly1);
	LL_init_list(&poly2);

	char buffer[1024];

	FILE *file = fmemopen(buffer, 1024, "w");
	REQUIRE(NULL != file);

	// Récupération des polynôme via. fichiers
	LL_create_list_fromFileName(&poly1, "./folder/poly1.txt");

	// Addition : P1 + P2
	poly_add(&poly1, &poly2);
	LL_save_list_toFile(file, poly1, monom_save2file);

	CHECK(0 == strcmp(buffer, "1.000 1\n-1.500 2\n1.000 3\n1.000 5\n"));
	CHECK(poly2 == NULL);

	LL_free_list(&poly1);

	printf("\nAddition d'un polynome nul avec un polynome non nul ...\n");
	cell_t *poly3;
	cell_t *poly4;

	LL_init_list(&poly3);
	LL_init_list(&poly4);

	char buffer1[1024];

	FILE *file1 = fmemopen(buffer1, 1024, "w");
	REQUIRE(NULL != file1);

	// Récupération des polynôme via. fichiers
	LL_create_list_fromFileName(&poly4, "./folder/poly1.txt");

	// Addition : P3 + P4
	poly_add(&poly3, &poly4);
	LL_save_list_toFile(file1, poly3, monom_save2file);

	CHECK(0 == strcmp(buffer1, "1.000 1\n-1.500 2\n1.000 3\n1.000 5\n"));
	CHECK(poly4 == NULL);

	LL_free_list(&poly3);

}

// Addition du polynome nul avec le polynome nul
TEST(Poly_addition3)
{
	printf("\nAddition de deux polynomes nuls\n");
	cell_t *poly1;
	cell_t *poly2;

	LL_init_list(&poly1);
	LL_init_list(&poly2);

	poly_add(&poly1, &poly2);

	CHECK(NULL == poly1);
	CHECK(poly2 == NULL);

	poly_add(&poly2, &poly1);

	CHECK(NULL == poly2);
	CHECK(poly1 == NULL);

}

// Addition d'un polynome avec un autre polynome dont les coefficients sont opposés
TEST(Poly_addition4)
{
	printf("\nAddition de deux polynomes opposés\n");
	cell_t *poly1;
	cell_t *poly2;

	LL_init_list(&poly1);
	LL_init_list(&poly2);

	LL_create_list_fromFileName(&poly1, "./folder/poly1.txt");

	LL_create_list_fromFileName(&poly2, "./folder/poly2.txt");

	poly_add(&poly1, &poly2);

	// on vérifie que l'addition des deux polynomes donne un résultat nul
	CHECK(NULL == poly1);
	CHECK(NULL == poly2);
}




TEST(Poly_produit) { // test sur le calcul du produit de deux polymones

	printf("\n\nMULTIPLYING\n");



// initialisation 

	cell_t *poly1, 	// polynôme 1
		   *poly2, 	// polynôme 2
		   *poly3; 	// polynôme de sortie 
	
	FILE   *file;
	char   	buffer[1024];

	poly3  	= NULL;
	file	= NULL;

// Multiplication des 2 polynôme nuls

	printf("\nMultiplying of 2 linked list null : \n");

	LL_init_list(&poly1);
	LL_init_list(&poly2);

	poly3 = poly_prod(poly1, poly2);

	CHECK( poly3 == NULL );

// Multiplication du polynôme 1 null et polynôme 2 non null

	printf("\nMultiplying of a linked list null and a linked list : \n");

	LL_init_list(&poly1);
	LL_init_list(&poly2);

	LL_create_list_fromFileName(&poly2, "folder/poly2.txt");			// affectation des valeurs aux polynômes 

	poly3 = poly_prod(poly1, poly2);

	CHECK( poly3 == NULL );

	LL_free_list(&poly2);
	free(poly3);

// Multiplication du polynôme 1 non nul et du polynôme 2 null

	printf("\nMultiplying of a linked list and a linked list null : \n");

	poly2 = NULL;
	LL_create_list_fromFileName(&poly1, "folder/poly1.txt");			// affectation des valeurs aux polynômes 

	poly3 = poly_prod(poly1, poly2);

	CHECK( poly3 == NULL );												// vérification que le polynôme 3 après la multiplication des polynômes

	LL_free_list(&poly1);
	LL_free_list(&poly2);

	
// Multiplication de 2 polynômes quelconques

	printf("\nMultiplying of 2 linked lists : \n");

	LL_init_list(&poly1);
	LL_init_list(&poly3);

	LL_create_list_fromFileName(&poly1, "folder/poly1.txt");    	// affectation des valeurs aux polynômes 
	LL_create_list_fromFileName(&poly3, "folder/poly3.txt");
	
	poly2 = poly_prod(poly1, poly3);

	// vérification du polynôme avec un buffer 

	file = fmemopen(buffer, 1024, "w");

	LL_save_list_toFile(file, poly2, *(monom_save2file));

	CHECK( 0 == strcmp(buffer, "1.000 1\n-0.500 2\n-0.500 3\n1.000 4\n1.000 5\n2.700 6\n-1.550 7\n0.200 8\n1.000 9\n1.700 10\n1.000 11\n2.545 18\n-3.817 19\n2.545 20\n2.545 22\n"));


	// Libération des polynômes
	LL_free_list(&poly1);
	LL_free_list(&poly2);
	LL_free_list(&poly3);

}


END_TEST_GROUP(polynomial)

int main(void) {
	RUN_TEST_GROUP(polynomial);
	return EXIT_SUCCESS;
}
