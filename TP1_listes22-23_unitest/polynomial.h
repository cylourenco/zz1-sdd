#ifndef __POLYNOMIAL_H__
#define __POLYNOMIAL_H__

// #include "linkedList.h"

/**
 * @brief compute 'in place' the derive of a polynomial
 * @param [in, out] adrPolyPt address of a polynomial's head pointer
 */
void poly_derive(cell_t ** adrPolyPt);

/** 
 * @brief compute P1 = P1 + P2, P2 become empty
 * @param adrFirstPolyPt [in, out] address of the 1st polynomial's head pointer
 * @param adrSecondPolyPt [in, out] address of the 2nd polynomial's head pointer
 */
void poly_add( cell_t ** adrFirstPolyPt, cell_t ** adrSecondPolyPt );

/** 
 * @brief P1 * P2
 * @param adrFirstPolyPt [in, out] head pointer of the 1st polynomial
 * @param adrSecondPolyPt [in, out] head pointer of the 2nd polynomial
 * @return P1*P2
 */
cell_t * poly_prod(cell_t * adrFirstPolyPt, cell_t * adrSecondPolyPt);

#endif