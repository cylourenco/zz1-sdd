/**
 * @file linkedList.c
 * @brief Implementation file of linked list module
 */
#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"

/**
 * @fn void LL_init_list(cell_t **adrHeadPt)
 * @brief Initialize a void list
 * @param [in, out] adrHeadPt address of head pointer of the list 
 */
void LL_init_list(cell_t **adrHeadPt)
{
    *adrHeadPt = NULL; // on initialise le pointeur sur la tête de la liste à NULL
}


/**
 * @brief Create a new cell for linked list from its data 
 * @param [in] monom address of the data
 * @return address of the new cell
 */
cell_t * LL_create_cell(monom_t * monom)
{
    cell_t * newCell;                           // cellule que l'on veut créer à partie de monom
    
    newCell = (cell_t *)malloc(sizeof(cell_t)); // allocation dynamique de la nouvelle cellule

    if (newCell == NULL)                        // si l'allocation n'a pas fonctionné on l'affiche sur la sortie standard
    {                                           // et on sort de la fonction avec 'exit'
        printf("Allocation malloc == NULL");
        exit(EXIT_FAILURE);
    }

    newCell -> val  = *monom;                   // on affecte comme valeur à la nouvelle cellule la valeur du monome donné en paramètre
    
    newCell -> next = NULL;                     // on met le suivant de la cellule à NULL

    return newCell;
}

/**
 * @brief Insert a cell into a linked list at the given position
 * @param [in, out]  adrPrevPt address of previous pointer of the cell
 * @param [in]  adrAdd address of the cell to be added to the linked list
 */
void LL_add_cell(cell_t ** adrPrevPt, cell_t * adrAdd)
{
    adrAdd->next = *adrPrevPt;                  // Pour ajouter la nouvelle cellule, on donne au suivant de cette dernière le suivant de
                                                // la cellule 'adrPrevPt'
    *adrPrevPt   =  adrAdd;                     // Et le 'nouveau' suivant de 'adrPrevPt' devient la cellule que l'on souhaite ajouter
}


/**
 * @brief Create a linked list from a file (in which the data is sorted)
 * @param [in, out] adrHeadPt address of head pointer of a linked list
 * @param [in] nameFile name of a file containing the data for a linked list
 * @return head pointer of the linked list
 */

cell_t * LL_create_list_fromFileName(cell_t ** adrHeadPt, char * nameFile)
{
    cell_t * headTemp = *adrHeadPt;

    FILE * file = fopen(nameFile, "r");          // On crée un fichier en utilisant son nom passé en paramètre

    if (file)                                    // On teste si le fichier existe, dans le cas contraire on affiche sur la sortie standard
    {                                            // que l'on ne peut pas l'utiliser

        LL_init_list(adrHeadPt);                 // On initialise la liste passée en paramètre à NULL  

        monom_t monom;                           // création d'un monome qui permettra de récupérer chaque monome du fichier 
        cell_t * newCell;

        // on récupère chaque monome à l'aide d'un scanf qui permet de stocker les valeurs dans 'monom' jusqu'à la fin du fichier : 'EOF'
        while(fscanf(file, "%lf %d\n", &monom.coef, &monom.degree) != EOF){

            newCell = LL_create_cell(&monom);
                                                 // on crée une nouvelle cellule permettant d'être ajouter à la liste, ayant les valeurs de 'monom'
            LL_add_cell(adrHeadPt, newCell);     // on ajoute cette nouvelle cellule dans l'ordre croissant des degrés

            adrHeadPt = &(*adrHeadPt) -> next;   // on avance dans la liste en passant au suivant, ce qui nous permettra d'ajouter de nouvelles cellules

        }
        
        fclose(file);                            // on ferme le fichier après avoir fini de la parcourir

    } 
    else 
    {
        LL_init_list(adrHeadPt);
        printf("On ne peut pas utiliser ce fichier\n");
    }

    return headTemp;                           // on retourne l'adresse de la tête de liste
}



/**
 * @brief Write the linked list to an output stream
 * @param [in] file file pointer of an output stream
 * @param [in] headPt head pointer of a linked list
 * @param function fonction pointer for printing the data of a cell on an output stream
 */

void LL_save_list_toFile(FILE * file, cell_t * headPt, void (*function)())
{
    if(file)                                    // on vérifie que le fichier existe, si ce n'est pas le cas on l'affiche
    {
        cell_t * cour = headPt;                 // création de cour qui va permettre de parcourir la liste sans la modifier

        while(cour != NULL)                     // tant qu'on n'atteint pas la fin de la liste
        {
            function(file, &cour -> val);       // on ajoute la valeur de la cellule sur laquelle on est dans le fichier donné en paramètre
            cour = cour -> next;                // on avance dans la liste en passant à la cellule suivante
        }

        fclose(file);                           // fermeture du fichier
        
    }
    else printf("On ne peut pas ouvrir le fichier\n");
}

/**
 * @brief Save a linked list into a file
 * @param [in, out] headPt head pointer of a linked list
 * @param [in] nameFile name of the backup file
 * @param function fonction pointer for writing the data of a cell to a output stream
 */
void LL_save_list_toFileName(cell_t * headPt, char * nameFile, void (*function)())
{
    if(headPt)                                  // si la tête de liste est différente de NULL
    {
        FILE * file = fopen(nameFile, "w");     // on crée un fichier à l'aide de la chaîne de caractères donnée en paramètre

        if(file)
            LL_save_list_toFile(file, headPt, function);  // on utilise la fonction précédente car l'écriture est similaire

        else 
            printf("On ne peut pas utiliser ce fichier\n");

                                                        
    }
}


/**
 * @brief Search a value in a linked list, and return the address of the previous pointer
 * @param [in] adrHeadPt address of the head pointer
 * @param [in] monom address of the value to search
 * @param  function fonction pointer for comparison of two values
 * @return the address of the previous pointer
 */

cell_t ** LL_search_prev(cell_t ** adrHeadPt, monom_t * monom, int (*function)())
{
    while (*adrHeadPt && (function(&((*adrHeadPt)->val), monom) < 0))
    {                                           // tant que l'adresse pointée n'est pas égale à NULL
                                                // et que le degré de la cellule dont l'adresse est pointée est inférieur au degré du monome
                                                // passé en paramètre (on a un ordre croissant dans les degrés de la liste)
    
            adrHeadPt = &((*adrHeadPt)->next);  // on avance dans la liste en passant à la cellule suivante
    }

    return adrHeadPt;                           // on retourne l'adresse sur le pointeur de la cellule trouvée
}


/**
 * @brief Delete a cell from a linked list
 * @param [in, out] adrPrevPt address of the previous pointer of the cell to delete
 */
void LL_del_cell(cell_t ** adrPrevPt)
{
    if(*adrPrevPt)                              // si le pointeur donné est égale à NULL on ne peut pas supprimer la cellule
    {
        cell_t * adrDelete;                     // on crée un pointeur sur la cellule à supprimer (suivant de celle donnée en paramètre)
        
        adrDelete  = *adrPrevPt;
        *adrPrevPt =  adrDelete -> next;        // on change le suivant de la cellule donnée en lui donnant le suivant de la cellule à supprimée
                                                // automatiquement la cellule n'existera plus dans la liste
        free(adrDelete);                        // on supprime cette cellule de la mémoire
    }

}

/**
 * @brief Display every cell of a linked list 
 * @param [in, out] headPt head pointer of a linked list
*/
void LL_display_list(cell_t * headPt)
{
    cell_t * cour;                              // création d'une cellule qui va parcourir la liste
    int number;                                 // numéro de la place de la cellule dans la liste 
    
    cour   = headPt;                            // on débute à l'adresse de la tête de liste
    number = 0;                                 // numéro de la 1ère cellule = 1

    while(cour)                                 // tant que la liste n'a pas été parcourue entièrement
    {
        printf("Element %d : %.2lf %d, suiv : %p\n", number, cour->val.coef, cour->val.degree, cour->next);
                            
        number += 1;                            // on incrémente le numéro de la cellule sur laquelle on se trouve
        cour    = cour -> next;                 // on avance dans la liste en passant à la cellule suivante
    }

}

/**
 * @brief Free the memory location occupied by a linked list
 * @param [in, out] adrHeadPt address of head pointer of a linked list
 */
void LL_free_list(cell_t ** adrHeadPt)
{
    while(*adrHeadPt)
    {
        LL_del_cell(adrHeadPt);
    }
}
