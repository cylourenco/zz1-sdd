#include <stdio.h>
#include <stdlib.h>
//#include <float.h>  // FLT_EPSILON DBL_EPSILON LDBL_EPSILON
//#include <math.h>

#include "linkedList.h"

/** 
 * @brief compute 'in place' the derive of a polynomial 
 * @param [in, out] adrPolyPt address of a polynomial's head pointer
 */
void poly_derive(cell_t ** adrPolyPt)
{
    // Initialisation 

    cell_t **   adrCour;    // adresse courante du polynôme

    adrCour = adrPolyPt;
                
    if( (*adrCour) != NULL && (*adrCour)->next == NULL && (*adrCour)->val.degree == 0 )     // cas où la seule valeur du polynôme est un degré égal à 0
    {
        //LL_del_cell(adrCour);
        *adrPolyPt = NULL;
    }

    //pour chaque cellule du polynôme, 
    //  - on multiplie son coef et son degree actuel 
    //  - on décémente le degré de -1

    while((*adrCour) != NULL)
    {
        if( (*adrCour)->val.degree != 0)
        {
            (*adrCour)->val.coef    = (*adrCour)->val.coef * (*adrCour)->val.degree;
            (*adrCour)->val.degree  -= 1;
        }
        else            // cas où le degrée est égal à 0 -> on supprime la cellule
        {
            LL_del_cell(adrCour);
        }

        adrCour = &(*adrCour)->next;
    }
}

/** 
 * @brief compute P1 = P1 + P2, P2 become empty
 * @param adrFirstPolyPt [in, out] address of the 1st polynomial's head pointer
 * @param adrSecondPolyPt [in, out] address of the 2nd polynomial's head pointer
 */
void poly_add( cell_t ** adrFirstPolyPt, cell_t ** adrSecondPolyPt )
{
    // Initialisation 

    cell_t ** adrSecondPolyPtMem,   // adresse du 2ème polynôme gardé en mémoire
            * new;                  // nouvelle cellule
    int     compare;                // entier qui compare le degrée de 2 monômes

    adrSecondPolyPtMem  = adrSecondPolyPt;
    new                 = NULL;

    while(*adrFirstPolyPt  && *adrSecondPolyPt)             // Tant qu'aucun monôme ne pointe sur NULL
    {
        compare = monom_degree_cmp(&(*adrFirstPolyPt)->val, &(*adrSecondPolyPt)->val);      // on garde en mémoire l'entier qui compare le degrée de 2 monômes

        if(compare == 0)                                    // si le 2 degrés sont égaux
        {
            if((*adrFirstPolyPt)->val.coef == -(*adrSecondPolyPt)->val.coef)                // si les coeffiecients s'annulent entre eux -> la cellule du 1er polynôme est supprimée
            {
                LL_del_cell(adrFirstPolyPt);
            }

            else                                            // sinon on additionne et on passe au monome suivant du 1er polynôme
            {
                (*adrFirstPolyPt)->val.coef += (*adrSecondPolyPt)->val.coef;
                adrFirstPolyPt              = &(*adrFirstPolyPt)->next;
            }

            LL_del_cell(adrSecondPolyPt);                   // on supprime le monôme du 2nd polynôme

        }
        else if(compare > 0)                                // si degré p1 > degré p2 alors on insère le monôme du 2nd polynôme devant le monôme du 1er pylynôme
        {
            new             = (*adrSecondPolyPt)->next;
            LL_add_cell(adrFirstPolyPt, *adrSecondPolyPt);
            adrSecondPolyPt = &new;
        }
        else                                                // si degré p1 < degré p2, on passe au monôme suivant du 1er polynome
        {
            adrFirstPolyPt = &(*adrFirstPolyPt)->next;
        }
    }
    
    if(!*adrFirstPolyPt && *adrSecondPolyPt)                // cas où le polynome 1 est nul et que le polynôme 2 est non null -> on finit de parcourir le polynome 1 avant la fin du 2
    {
        *adrFirstPolyPt = *adrSecondPolyPt;
    }

    *adrSecondPolyPtMem = NULL;                             //suppression du deuxième polynome
}



/** 
 * @brief compute P1 * P2
 * @param adrFirstPolyPt [in, out] head pointer of the 1st polynomial
 * @param adrSecondPolyPt [in, out] head pointer of the 2nd polynomial
 * @return P1*P2
 */
cell_t * poly_prod(cell_t * adrFirstPolyPt, cell_t * adrSecondPolyPt)
{
    // initialisation 

	cell_t  *  adrPolyProd,          // polynôme de retour
            *  adrFirstPolyCour,     // adresse courante de adrFirstPolyPt
            *  adrSecondPolyCour,    // adresse courante de adrSecondPolyPt
            *  cellNew,              // nouvelle celulle
            ** adrPolyProdPrev;     // adresse du polynôme precedent du pointeur courant de adrPolyProd

    monom_t monomNew;               // nouveau monome crée pour insérer dans la liste

    LL_init_list(&adrPolyProd);
    LL_init_list(&cellNew);
    adrFirstPolyCour    = adrFirstPolyPt;
    adrSecondPolyCour   = adrSecondPolyPt;

    while(adrFirstPolyCour != NULL)
    {
        while(adrSecondPolyCour != NULL)
        {
            monomNew.coef       = adrFirstPolyCour->val.coef    * adrSecondPolyCour->val.coef;      // on multiplie les coef
            monomNew.degree     = adrFirstPolyCour->val.degree  + adrSecondPolyCour->val.degree;    // et on additionne les degrees dans une nouvelle cellule

            adrPolyProdPrev = LL_search_prev(&adrPolyProd, &monomNew, *(monom_degree_cmp));         // on cherche le precedent du polynôme

            if((*adrPolyProdPrev) != NULL && (*adrPolyProdPrev)->val.degree == monomNew.degree)     // si le polynome de sortie possède le même degrée que la nouvelle cellule on additionne les coef
            {
                (*adrPolyProdPrev)->val.coef += monomNew.coef;
            
                if((*adrPolyProdPrev)->val.coef == 0)                                               // si la somme des 2 coef est nulle alors on supprime la cellule qui est à 0 sinon on ne fait rien
                {
                    LL_del_cell(adrPolyProdPrev);
                }
            }

            else                                                                                    // sinon une nouvelle cellule est crée et est ajouté dans le polynôme de sortie
            {
                cellNew = LL_create_cell(&monomNew);
                LL_add_cell(adrPolyProdPrev, cellNew);
            }

            adrSecondPolyCour   = adrSecondPolyCour->next;                                          // on passe à la cellule suivante du 2ème polynôme 
        }

        adrFirstPolyCour    = adrFirstPolyCour->next;                                               // on passe à la cellule suivante du 1er polynôme 
        adrSecondPolyCour   = adrSecondPolyPt;                                                      // et on rembobine le 2ème polynôme 

    }

    return adrPolyProd;
}




