/**
 * @file linkedList.h
 * @brief Header file of linked list module
 */

#ifndef __LINKEDLIST_H__
#define __LINKEDLIST_H__

#include "valCell.h"

typedef struct cell_t cell_t;

struct cell_t{
    monom_t val;
    cell_t * next;
};


/**
 * @fn void LL_init_list(cell_t **adrHeadPt)
 * @brief Initialize a void list
 * @param [in, out] adrHeadPt address of head pointer of the list 
 */
void LL_init_list(cell_t **adrHeadPt);

/**
 * @brief Create a new cell for linked list from its data 
 * @param [in] monom address of the data
 * @return address of the new cell
 */
cell_t * LL_create_cell(monom_t * m);

/**
 * @brief Insert a cell into a linked list at the given position
 * @param [in, out]  adrPrevPt address of previous pointer of the cell
 * @param [in]  adrAdd address of the cell to be added to the linked list
 */
void LL_add_cell(cell_t ** adrPrevPt, cell_t * adrAdd);

/**
 * @brief Create a linked list from a file (in which the data is sorted)
 * @param [in, out] adrHeadPt address of head pointer of a linked list
 * @param [in] nameFile name of a file containing the data for a linked list
 * @return head pointer of the linked list
 */
cell_t * LL_create_list_fromFileName(cell_t ** adrHeadPt, char * nameFile);

/**
 * @brief Write the linked list to an output stream
 * @param [in] file file pointer of an output stream
 * @param [in] headPt head pointer of a linked list
 * @param function fonction pointer for printing the data of a cell on an output stream
 */
void LL_save_list_toFile(FILE * file, cell_t * headPt, void (*function)());

/**
 * @brief Save a linked list into a file
 * @param [in, out] headPt head pointer of a linked list
 * @param [in] nameFile name of the backup file
 * @param function fonction pointer for writing the data of a cell to a output stream
 */
void LL_save_list_toFileName(cell_t * headPt, char * nameFile, void (*function)());

/**
 * @brief Search a value in a linked list, and return the address of the previous pointer
 * @param [in] adrHeadPt address of the head pointer
 * @param [in] monom address of the value to search
 * @param  function fonction pointer for comparison of two values
 * @return the address of the previous pointer
 */

cell_t ** LL_search_prev(cell_t ** adrHeadPt, monom_t * monom, int (*function)());

/**
 * @brief Delete a cell from a linked list
 * @param [in, out] adrPrevPt address of the previous pointer of the cell to delete
 */
void LL_del_cell(cell_t ** adrPrevPt);


/**
 * @brief Display every cell of a linked list 
 * @param [in, out] headPt head pointer of a linked list
*/
void LL_display_list(cell_t * headPt);

/**
 * @brief Free the memory location occupied by a linked list
 * @param [in, out] adrHeadPt address of head pointer of a linked list
 */
void LL_free_list(cell_t ** adrHeadPt);


#endif