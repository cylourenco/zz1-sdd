# TP1 - Listes Chaînées et polynômes

L'objet de ce TP est de savoir manipuler les polynômes en utilisant les listes chaînées en C. Ici, il faut savoir manipuler les polynômes en utilisant la meilleure complexité possible et en respectant les contraintes de la liste des polynômes.

Pour plus de détails du projet, lisez le pdf.

## Compilation

Pour compiler juste la version avec les listes chaînées :  
$make linkedList  
$./linkedList  

Pour compiler juste la version avec les polynômes :  
$make polynomial  
$./polynomial  

## Auteurs  
* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/cylourenco)
* **Louane MECHOUD** _alias_ [@lomechoud](https://gitlab.isima.fr/lomechoud)
