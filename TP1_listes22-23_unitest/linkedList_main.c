/**
 * @file linkedList_main.c
 * @brief Programme pour les tests de fonctions de gestion de liste chainee
 * ATTENTION : Il faut creer autant de tests que les cas d'execution pour chaque fonction a tester
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedList.h"
#include "teZZt.h"
#include "valCell.h"

BEGIN_TEST_GROUP(linkedList)

TEST(monom_degree_cmp) {
	monom_t v1 = {5.11, 7};
	monom_t v2 = {3., 5};
	monom_t v3 = {5.25, 7};

	printf("\nComparaison des monomes : \n");
	CHECK( monom_degree_cmp(&v1, &v2) > 0 );
	CHECK( monom_degree_cmp(&v2, &v1) < 0 );
	CHECK( monom_degree_cmp(&v1, &v3) == 0 );
}

TEST(monom_save2file) {
	printf("\n------------------------------------------\n");

/*
	Liste de cas:
		- le cas où le monôme est null
		- le cas où le monôme n'est pas null
*/

    // initialisation
    monom_t  	v = {5., 7};
    FILE      *	file;
    char        buffer[1024];

    printf("\nSave a null monom in a file : \n");
    
    file = fmemopen(buffer, 1024, "w");                // creation du flux de texte => buffer
    REQUIRE ( NULL != file);

    monom_save2file(file, NULL);

    fclose(file);
    CHECK( 0 == strcmp(buffer, " ") );                 // vérification avec le buffer

    printf("\nSave a monom in a file : \n");

    file = fmemopen(buffer, 1024, "w");                // creation du flux de texte => buffer
    REQUIRE ( NULL != file);

    monom_save2file(file, &v);

    fclose(file);
    CHECK( 0 == strcmp(buffer, "5.000 7\n") );         // vérification avec le buffer
}


TEST(LL_init_list) {
	printf("\n------------------------------------------\n");

	cell_t *list;

	printf("\nInitialization of the linked list : \n");
	LL_init_list(&list);

	REQUIRE ( list == NULL );
}


TEST(LL_create_cell) { // test de creation de cellule
	printf("\n------------------------------------------\n");

	cell_t *new = NULL;
	monom_t m1 = {3.245, 17};

	printf("\nCreate a new cell (3.245 17) : \n");
	new = LL_create_cell(&m1);
	REQUIRE ( NULL != new );
	
	CHECK ( NULL == new->next );

	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE ( NULL != file);

	monom_save2file(file, &(new->val));
	fclose(file);
	CHECK( 0 == strcmp(buffer, "3.245 17\n") ); 


	free(new);

}

// test d'insertion de cellule - liste a une cellule
TEST(LL_add_cell1) { 
	printf("\n------------------------------------------\n");

	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.45, 17};

	printf("\nAdd a cell to a linked list : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );

	LL_add_cell(&list, new);
	CHECK( list == new ); 

	CHECK( list->val.coef == 3.45 );  // 3.45 est une valeur non approchee
	CHECK( list->val.degree == 17 );  
	CHECK( list->next == NULL );

	free(list); // liberer la cellule
	list = NULL;

}

// test d'insertion de cellule - liste a deux cellules
TEST(LL_add_cell2) { 
	printf("\n------------------------------------------\n");

	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.45, 17};
	monom_t m2 = {25.8, 9};

	printf("\nAdd two cells to a linked list : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	CHECK( list->val.coef == 25.8 );
	CHECK( list->val.degree == 9 );  
	CHECK( list->next != NULL );

	CHECK( list->next->val.coef == 3.45 );  // 3.45 est une valeur non approchee
	CHECK( list->next->val.degree == 17 );  
	CHECK( list->next->next == NULL );

	LL_free_list(&list);
	CHECK( list == NULL );
}

// test d'insertion de cellule - liste a trois cellules
TEST(LL_add_cell3) { 
	printf("\n------------------------------------------\n");

	cell_t *list = NULL;
	cell_t *new = NULL;
	monom_t m1 = {3.245, 17};
	monom_t m2 = {25.8, 9};
	monom_t m3 = {12.4, 3};

	printf("\nAdd three cells to a linked list : \n");

	new = LL_create_cell(&m1);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m2);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	new = LL_create_cell(&m3);
	REQUIRE ( new != NULL );
	LL_add_cell(&list, new);
	CHECK( list == new ); 

	CHECK( list->val.coef == 12.4 );
	CHECK( list->val.degree == 3 );  
	CHECK( list->next != NULL );

	CHECK( list->next->val.coef == 25.8 );
	CHECK( list->next->val.degree == 9 );  
	CHECK( list->next->next != NULL );

	CHECK( list->next->next->val.coef == 3.245 );  
	CHECK( list->next->next->val.degree == 17 );  
	CHECK( list->next->next->next == NULL );

	LL_free_list(&list);
	CHECK( list == NULL );
}


// test pour la creation d'un polynome a partir d'un fichier - exemple
TEST(LL_create_list_fromFileName0) {
	printf("\n------------------------------------------\n");

	cell_t *list;

	printf("\nCreate a linked list from file name0: \n");

	LL_create_list_fromFileName(&list, "notExist.txt");
	CHECK( NULL == list );


}

// test pour la creation d'un polynome a partir d'un fichier
TEST(LL_create_list_fromFileName) {
	printf("\n------------------------------------------\n");

	cell_t *list, *listCmp;
	cell_t *listT, *listCmpT;

	LL_init_list(&list);
	LL_init_list(&listCmp);

	cell_t *new = NULL;
	monom_t m4 = {3.245, 17};
	monom_t m5 = {25.8, 9};
	monom_t m6 = {12.4, 3};
	monom_t m3 = {5.521, 18};
	monom_t m2 = {15.1, 25};
	monom_t m1 = {54.251, 40};

	new = LL_create_cell(&m1);
	LL_add_cell(&listCmp, new);

	new = LL_create_cell(&m2);
	LL_add_cell(&listCmp, new);

	new = LL_create_cell(&m3);
	LL_add_cell(&listCmp, new);

	new = LL_create_cell(&m4);
	LL_add_cell(&listCmp, new);	

	new = LL_create_cell(&m5);
	LL_add_cell(&listCmp, new);

	new = LL_create_cell(&m6);
	LL_add_cell(&listCmp, new);	

	printf("\nCreate a linked list from file name1 : \n");

	LL_create_list_fromFileName(&list, "folder/list.txt");

	CHECK( list != NULL ); 
	
	int i;

	listT = list;
	listCmpT = listCmp;

	for (i = 0; i < 6; i++)
	{
		CHECK( monom_degree_cmp( &listCmpT -> val, &listT -> val ) == 0 );
		listT = listT -> next;
		listCmpT = listCmpT -> next;
	}

	LL_free_list(&list);
	LL_free_list(&listCmp);
	CHECK( list == NULL );

}

// test pour l'affichage d'une liste
TEST(LL_display_list) {
	cell_t *list = NULL;
	cell_t *list2 = NULL;

	printf("\n------------------------------------------\n");


	// test sur une liste vide
	CHECK( NULL == list );
	printf("\nDisplay an empty linked list : \n");
	LL_display_list(list);

	// test sur une liste non vide
	LL_create_list_fromFileName(&list2, "folder/list.txt");
	CHECK( NULL != list2 );
	printf("\nDisplay a linked list : \n");
	LL_display_list(list2);

	LL_free_list(&list2);
	CHECK( list2 == NULL );

}


TEST(LL_save_list_toFile) { // test pour l'ecriture d'un polynome sur un flux de sortie
	printf("\n------------------------------------------\n");

	cell_t *list;
	
	LL_create_list_fromFileName(&list, "folder/list.txt");

	printf("\nSave a linked list to File : \n");

	// creation du flux de texte => buffer
	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE (NULL != file);

	LL_save_list_toFile(file, list, monom_save2file);

	CHECK( 0 == strcmp(buffer, "12.400 3\n25.800 9\n3.245 17\n5.521 18\n15.100 25\n54.251 40\n") );

	LL_free_list(&list);
	CHECK( list == NULL );

}

TEST(LL_search_prev) { // test pour la fonction de recherche d'une valeur
	printf("\n------------------------------------------\n");
	
	cell_t *list;
	cell_t **cellPrev;
	monom_t monom = {15.1, 25};
	monom_t monom2 = {2.3, 15};

	printf("\nSearch in a linked list a monom : \n");

	LL_create_list_fromFileName(&list, "folder/list.txt");

	printf("\nListe dans laquelle on recherche la cellule\n");
	LL_display_list(list);


	printf("\nLa cellule n'existe pas dans la liste :\n");
	cellPrev = LL_search_prev(&list, &monom2, monom_degree_cmp);
	printf("Degré de la cellule demandée : %d\n", (&monom2)->degree);

	CHECK( (*cellPrev)->val.coef == 3.245 );
	CHECK( (*cellPrev)->val.degree == 17 );


	printf("\nLa cellule existe dans la liste :\n");
	cellPrev = LL_search_prev(&list, &monom, monom_degree_cmp);
	printf("Degré de la cellule trouvée grâce à la fonction : %d\n", (*cellPrev)->val.degree);

	CHECK( (*cellPrev)->val.coef == 15.1 );
	CHECK( (*cellPrev)->val.degree == 25 );

	printf("\nSearch in a linked list a monom which is the first : \n");

	monom.degree = 3;

	cellPrev = LL_search_prev(&list, &monom, *monom_degree_cmp);
	printf("Degré de la cellule trouvée grâce à la fonction : %d\n", (*cellPrev)->val.degree);


	CHECK( (*cellPrev)->val.coef == 12.4 );
	CHECK( (*cellPrev)->val.degree == 3 );

	printf("\nSearch in a linked list a monom which is the last : \n");

	monom.degree = 40;

	cellPrev = LL_search_prev(&list, &monom, *monom_degree_cmp);
	printf("Degré de la cellule trouvée grâce à la fonction : %d\n", (*cellPrev)->val.degree);

	CHECK( (*cellPrev)->val.coef == 54.251 );
	CHECK( (*cellPrev)->val.degree == 40 );

	LL_free_list(&list);
	CHECK( list == NULL );

}

TEST(LL_add_celln) { // test d'insertion de cellule - liste a n cellules
	printf("\n------------------------------------------\n");
	
	cell_t *list = NULL;
	monom_t monom = {1, 1};

	LL_create_list_fromFileName(&list, "folder/list.txt");

	printf("\nAdd a linked list in n cell : \n");


	LL_add_cell(&list->next->next->next->next->next->next, LL_create_cell(&monom));
	LL_add_cell(&list->next->next->next->next->next, LL_create_cell(&monom));
	LL_add_cell(&list->next->next->next->next, LL_create_cell(&monom));
	LL_add_cell(&list->next->next->next, LL_create_cell(&monom));
	LL_add_cell(&list->next->next, LL_create_cell(&monom));
	LL_add_cell(&list->next, LL_create_cell(&monom));
	LL_add_cell(&list, LL_create_cell(&monom));



	char buffer[1024];
	FILE * file = fmemopen(buffer, 1024, "w");
	REQUIRE (NULL != file);

	LL_save_list_toFile(file, list, *(monom_save2file));
	
	CHECK( 0 == strcmp(buffer, "1.000 1\n12.400 3\n1.000 1\n25.800 9\n1.000 1\n3.245 17\n1.000 1\n5.521 18\n1.000 1\n15.100 25\n1.000 1\n54.251 40\n1.000 1\n") );

	LL_free_list(&list);
}

TEST(LL_del_cell) { // test de la suppression d'un element
	printf("\n------------------------------------------\n");

	cell_t 	*list,
			*listCopy;

	printf("\nDelete a cell in a linked list : \n");

	LL_create_list_fromFileName(&list, "folder/list.txt");
	LL_create_list_fromFileName(&listCopy, "folder/list.txt");

	printf("\nAffichage de la liste avant suppression des cellules\n");
	LL_display_list(list);


	printf("\nOn supprime la première cellule de la liste :\n");
	LL_del_cell(&list);

	CHECK( list -> next != listCopy -> next );
	CHECK( monom_degree_cmp(&list -> next -> val, &listCopy -> next -> next -> val ) == 0);

	printf("\nAffichage de la liste après suppression de la première cellule\n");
	LL_display_list(list);



	printf("\nOn supprime la troisième cellule de la liste :\n");
	LL_del_cell(&list->next->next);

	CHECK( list -> next -> next -> next != listCopy -> next -> next -> next );
	CHECK( monom_degree_cmp(&list -> next -> next -> next -> val, &listCopy -> next -> next -> next -> next -> next -> val ) == 0);

	printf("\nAffichage de la liste après suppression de la cellule\n");
	LL_display_list(list);


	printf("\nLa cellule n'existe pas\n");
	LL_del_cell(&list->next->next->next->next);

	// même test qu'avant l'appel à la fonction -> la tentative de suppression n'a rien modifié
	CHECK( list -> next -> next -> next != listCopy -> next -> next -> next );
	CHECK( monom_degree_cmp(&list -> next -> next -> next -> val, &listCopy -> next -> next -> next -> next -> next -> val ) == 0);

	printf("\nAffichage de la liste après suppression d'une cellule n'étant pas dans la liste\n");
	LL_display_list(list);


	LL_free_list(&list);
	LL_free_list(&listCopy);
	CHECK( list == NULL );
	
}

TEST(LL_free_list) { // test de la liberation de liste
	cell_t *list = NULL;

	printf("\nFree a linked list : \n");

	printf("\nListe Vide\n");
	LL_free_list(&list);
	CHECK( list == NULL );


	printf("\nListe avec du contenu\n");
	LL_create_list_fromFileName(&list, "folder/list.txt");

	LL_free_list(&list);
	CHECK( list == NULL );
	
}

TEST(LL_save_list_toFileName) { // BONUS - 3eme Seance
	printf("\n------------------------------------------\n");

	cell_t *list;

	LL_create_list_fromFileName(&list, "folder/list.txt");

	printf("\n\nSave a linked list to File Name : \n");

	cell_t * listCmp;
	cell_t * listCmp2;

	printf("\nTest avec un fichier qui n'existe pas\n");
	LL_save_list_toFileName(list, "folder/notExist1.txt", *monom_save2file);

	LL_create_list_fromFileName(&listCmp2, "folder/notExist1.txt");

	CHECK( list->val.coef == listCmp2->val.coef );
	CHECK( list->val.degree == listCmp2->val.degree );
	CHECK( list->next->val.coef == listCmp2->next->val.coef );
	CHECK( list->next->val.degree == listCmp2->next->val.degree );
	CHECK( list->next->next->val.coef == listCmp2->next->next->val.coef );
	CHECK( list->next->next->val.degree == listCmp2->next->next->val.degree );
	


	printf("Test avec un fichier qui existe\n");
	LL_save_list_toFileName(list, "folder/save.txt", *monom_save2file);

	LL_create_list_fromFileName(&listCmp, "folder/save.txt");

	CHECK( list->val.coef == listCmp->val.coef );
	CHECK( list->val.degree == listCmp->val.degree );
	CHECK( list->next->val.coef == listCmp->next->val.coef );
	CHECK( list->next->val.degree == listCmp->next->val.degree );
	CHECK( list->next->next->val.coef == listCmp->next->next->val.coef );
	CHECK( list->next->next->val.degree == listCmp->next->next->val.degree );

	LL_free_list(&list);

	LL_free_list(&listCmp2);

	LL_free_list(&listCmp);

}


END_TEST_GROUP(linkedList)

int main(void) {
	RUN_TEST_GROUP(linkedList);
	return EXIT_SUCCESS;
}
