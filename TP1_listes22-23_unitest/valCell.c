/**
 * @file valCell.c
 * Implementation file for comparison and display of linked list's cells
 */

#include <stdio.h>
#include "valCell.h"

/** 
 * @brief Compare the degree of two monomials
 * @param [in] m1 address of the first monomial
 * @param [in] m2 address of the second monomial
 * @return <0 if degree1<degree2; =0 if degree1=degree2;  >0 if degree1>degree2
 */ 
int monom_degree_cmp(monom_t * m1, monom_t * m2)
{
	int res = 0;                            // si le degré m1 est égale au polynôme m2 le résultat sera nul
    
    if (m1 -> degree < m2 -> degree)        // si le degré m1 est inférieur au polynôme m2 le résultat sera négatif
    {
        res -= 1;
    }
    else if (m1 -> degree > m2 -> degree)    // si le degré m1 est supérieur au polynôme m2 le résultat sera positif
    {
        res += 1;
    }

    return res;
}


/**
 * @brief write the information of a monomial to the given output stream
 * @param [in] file file pointer of an output stream
 * @param [in] monom address of a monomial
 */
void monom_save2file(FILE * file, monom_t * monom)
{
    if (file && monom)                                                           // si le fichier n'est pas nul
    {
        fprintf(file, "%.3lf %d\n", monom -> coef, monom -> degree);    // afficher dans le fichier le coef et le degré
    }

    else if (file && !monom)
    {
        fprintf(file, " ");
    }
}
